package base;

public abstract class Shape {
    public abstract double keliling(double x, double y);
    public abstract double luas(double x, double y);
}
