package base.shape;

import base.Shape;

public class Lingkaran extends Shape {
    private final double pi = 3.1415;
    @Override
    public double keliling(double x, double y) {
        return 0;
    }
    @Override
    public double luas(double x, double y) {
        return 0;
    }
    public double keliling(double r) {
        return (2 * pi * r);
    }
    public double luas(double r) {
        return (pi * r * r);
    }
}
