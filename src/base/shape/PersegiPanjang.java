package base.shape;

import base.Shape;

public class PersegiPanjang extends Shape {
    @Override
    public double keliling(double x, double y) {
        return 2 * (x + y);
    }

    @Override
    public double luas(double x, double y) {
        return x * y;
    }
}
