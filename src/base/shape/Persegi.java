package base.shape;

import base.Shape;

public class Persegi extends Shape {
    public double keliling(double x) {
        return 4 * x;
    }

    public double luas(double x) {
        return x * x;
    }

    @Override
    public double keliling(double x, double y) {
        return (x + y) * 2;
    }

    @Override
    public double luas(double x, double y) {
        return x * y;
    }
}
