package base.shape;

import base.Shape;

public class SegitigaSiku extends Shape {

    @Override
    public double keliling(double x, double y) {
        return (x + y + sisiMiring(x, y));
    }

    @Override
    public double luas(double x, double y) {
        return 0.5 * x * y;
    }

    public double sisiMiring(double s1, double s2) {
        return Math.sqrt((s1 * s1) + (s2 * s2));
    }
}
