package base.geometry;

import base.Geometry;

public class Kubus extends Geometry {
    double x;
    public double getX() {
        return x;
    }
    public void setX(double x) {
        this.x = x;
    }
    @Override
    public double volume() {
        return x * x * x;
    }
    @Override
    public double luasPermukaan() {
        return 2 * (x + x + x);
    }
}
