package base.geometry;

import base.Geometry;

public class Balok extends Geometry {
    double x, y ,z;

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    @Override
    public double volume() {
        return x * y * z;
    }

    @Override
    public double luasPermukaan() {
        return 2 * (x + y + z);
    }
}
