package base.geometry;

import base.Geometry;

public class Kerucut extends Geometry {
    private final double pi = 3.1415;
    double r, t;

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    public double getT() {
        return t;
    }

    public void setT(double t) {
        this.t = t;
    }

    @Override
    public double volume() {
        return (pi * r * r * t) / 3;
    }

    @Override
    public double luasPermukaan() {
        return (pi * r * sisiMiring(r, t)) + (pi * r * r);
    }
    public double sisiMiring(double s1, double s2) {
        return Math.sqrt((s1 * s1) + (s2 * s2));
    }
}
