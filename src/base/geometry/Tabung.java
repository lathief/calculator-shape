package base.geometry;

import base.Geometry;

public class Tabung extends Geometry {
    private final double pi = 3.1415;
    double r, t;

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    public double getT() {
        return t;
    }

    public void setT(double t) {
        this.t = t;
    }

    @Override
    public double volume() {
        return pi * r * r *t;
    }

    @Override
    public double luasPermukaan() {
        return  2 * pi * r * (r + t);
    }
}
