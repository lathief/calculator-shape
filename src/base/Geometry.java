package base;

public abstract class Geometry {
    public abstract double volume();
    public abstract double luasPermukaan();
}
