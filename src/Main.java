import base.geometry.Balok;
import base.geometry.Kerucut;
import base.geometry.Kubus;
import base.geometry.Tabung;
import base.shape.Lingkaran;
import base.shape.Persegi;
import base.shape.PersegiPanjang;
import base.shape.SegitigaSiku;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    private static final Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        mainMenu();
    }
    private static void mainMenu() {
        //pilihan menu
        System.out.println("================================================");
        System.out.println("| Aplikasi penghitung bangun datar atau ruang  |");
        System.out.println("================================================");
        System.out.println("1. Bangun Datar");
        System.out.println("2. Bangun Ruang");
        System.out.println("0. Keluar\n");
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader bufferedReader = new BufferedReader(isr);
        try {
            System.out.print("Masukan Pilihan Bangun : ");
            String input = bufferedReader.readLine();
            if (input.isEmpty()) {
                System.out.println("\nAnda tidak memasukkan inputan, silahkan coba lagi.\n");
                mainMenu();
            } else {
                if (input.equals("1")){
                    bangunDatar();
                } else if (input.equals("2")){
                    bangunRuang();
                } else if (input.equals("0")){
                    System.out.println("\nProgram dihentikan.\n");
                } else {
                    System.out.println("\nAnda tidak memasukkan inputan, silahkan coba lagi.\n");
                    mainMenu();
                }
            }
        } catch (NumberFormatException e) {
            System.out.println("\nTidak ada pilihan, silahkan coba lagi.\n");
            mainMenu();
        } catch (IOException e) {
            mainMenu();
            throw new RuntimeException(e);
        }
    }
    private static void bangunRuang(){
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader bufferedReader = new BufferedReader(isr);
        try {
            System.out.println("================================================");
            System.out.println("| Hitung Luas dan Keliling Bangun Ruang        |");
            System.out.println("================================================");
            System.out.println("1. Kubus");
            System.out.println("2. Balok");
            System.out.println("3. Kerucut");
            System.out.println("4. Tabung");
            System.out.println("0. Kembali ke menu awal\n");
            System.out.print("Masukan Pilihan Menu : ");

            String input = bufferedReader.readLine();
            if (input.isEmpty()) {
                System.out.println("\nAnda tidak memasukkan inputan, silahkan coba lagi.\n");
                bangunRuang();
            } else {
                int numberOfString = Integer.parseInt(input);
                switch (numberOfString){
                    case 1:
                        kubus();
                        break;
                    case 2:
                        balok();
                        break;
                    case 3:
                        kerucut();
                        break;
                    case 4:
                        tabung();
                        break;
                    case 0:
                        System.out.println("\nKembali ke menu awal.\n");
                        mainMenu();
                        break;
                    default:
                        System.out.println("\nTidak ada pilihan, silahkan coba lagi.\n");
                        bangunRuang();
                        break;
                }
            }
        } catch (NumberFormatException e){
            System.out.println("\nTidak ada pilihan, silahkan coba lagi.\n");
            bangunRuang();
        } catch (IOException e) {
            bangunRuang();
            throw new RuntimeException(e);

        }
    }
    private static void bangunDatar(){
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader bufferedReader = new BufferedReader(isr);
        try {
            System.out.println("================================================");
            System.out.println("| Hitung Luas dan Keliling Bangun Datar        |");
            System.out.println("================================================");
            System.out.println("1. Persegi");
            System.out.println("2. Persegi Panjang");
            System.out.println("3. Segitiga Siku-Siku");
            System.out.println("4. Lingkaran");
            System.out.println("0. Kembali ke menu awal\n");
            System.out.print("Masukan Pilihan Menu : ");
            String input = bufferedReader.readLine();
            if (input.isEmpty()) {
                System.out.println("\nAnda tidak memasukkan inputan, silahkan coba lagi.\n");
                bangunDatar();
            } else {
                int numberOfString = Integer.parseInt(input);
                switch (numberOfString){
                    case 1:
                        persegi();
                        break;
                    case 2:
                        persegiPanjang();
                        break;
                    case 3:
                        segitigaSiku();
                        break;
                    case 4:
                        lingkaran();
                        break;
                    case 0:
                        System.out.println("\nKembali ke menu awal.\n");
                        mainMenu();
                        break;
                    default:
                        System.out.println("\nTidak ada pilihan, silahkan coba lagi.\n");
                        bangunDatar();
                        break;
                }
            }
        } catch (NumberFormatException e){
            System.out.println("\nTidak ada pilihan, silahkan coba lagi.\n");
            bangunDatar();
        } catch (IOException e) {
            bangunDatar();
            throw new RuntimeException(e);

        }
    }

    
    private static void persegi() {
        /*Kamus*/
        Persegi opsg;
        double inputAngka;
        char in;

        opsg = new Persegi();

        do {
            System.out.println("================================================");
            System.out.println("| Persegi                                      |");
            System.out.println("================================================");
            System.out.print("Masukan sisi persegi: ");
            inputAngka = scanner.nextDouble();
            System.out.println("Luas persegi: " + opsg.luas(inputAngka));
            System.out.println("Keliling persegi: " + opsg.keliling(inputAngka));

            System.out.print("\nIngin mencoba lagi? (y/t) : ");
            in = scanner.next().charAt(0);
            System.out.print("\n");
        } while (in == 'Y' || in == 'y');

        System.out.print("\n");

        bangunDatar();
    }
    private static void persegiPanjang() {
        /*Kamus*/
        PersegiPanjang opsgpjg;
        double s1, s2;
        char in;

        opsgpjg = new PersegiPanjang();

        do {
            System.out.println("================================================");
            System.out.println("| Persegi Panjang                              |");
            System.out.println("================================================");
            System.out.print("Masukan panjang persegi panjang: ");
            s1 = scanner.nextDouble();
            System.out.print("Masukan lebar persegi panjang: ");
            s2 = scanner.nextDouble();
            System.out.println("Luas persegi panjang: " + opsgpjg.luas(s1, s2));
            System.out.println("Keliling persegi panjang: " + opsgpjg.keliling(s1, s2));
            System.out.print("\nIngin mencoba lagi? (y/t) : ");
            in = scanner.next().charAt(0);
            System.out.print("\n");
        } while (in == 'Y' || in == 'y');

        System.out.print("\n");

        bangunDatar();
    }
    private static void segitigaSiku() {
        /*Kamus*/
        SegitigaSiku ostgsiku;
        double s1, s2;
        char coba;

        ostgsiku = new SegitigaSiku();

        do {
            System.out.println("================================================");
            System.out.println("| Segitiga Siku siku                           |");
            System.out.println("================================================");
            System.out.print("Masukan alas segitiga siku-siku: ");
            s1 = scanner.nextDouble();
            System.out.print("Masukan tinggi segitiga siku-siku: ");
            s2 = scanner.nextDouble();
            System.out.println("Sisi miring (hipotenusa): " + ostgsiku.sisiMiring(s1, s2));
            System.out.println("Luas segitiga: " + ostgsiku.luas(s1, s2));
            System.out.println("Keliling segitiga: " + ostgsiku.keliling(s1, s2));

            System.out.print("\nIngin mencoba lagi? (y/t) : ");
            coba = scanner.next().charAt(0);
            System.out.print("\n");
        } while (coba == 'Y' || coba == 'y');

        System.out.print("\n");

        bangunDatar();
    }
    private static void lingkaran() {
        /*Kamus*/
        Lingkaran olingkaran;
        double r;
        char coba;

        olingkaran = new Lingkaran();

        do {
            System.out.println("================================================");
            System.out.println("| Lingkaran                                    |");
            System.out.println("================================================");
            System.out.print("Masukan jari-jari lingkaran: ");
            r = scanner.nextDouble();
            System.out.println("Luas lingkaran: " + olingkaran.luas(r));
            System.out.println("Keliling lingkaran: " + olingkaran.keliling(r));

            System.out.print("\nIngin mencoba lagi? (y/t) : ");
            coba = scanner.next().charAt(0);
            System.out.print("\n");
        } while (coba == 'Y' || coba == 'y');

        System.out.print("\n");

        bangunDatar();
    }
    private static void kubus() {
        Kubus kubus;
        double inputAngka;
        char in;

        kubus = new Kubus();
        do {
            System.out.println("================================================");
            System.out.println("| Kubus                                        |");
            System.out.println("================================================");
            System.out.print("Masukan sisi kubus: ");
            inputAngka = scanner.nextDouble();
            kubus.setX(inputAngka);
            System.out.println("Volume kubus: " + kubus.volume());
            System.out.println("Luas Permukaan kubus: " + kubus.luasPermukaan());

            System.out.print("\nIngin mencoba lagi? (y/t) : ");
            in = scanner.next().charAt(0);
            System.out.print("\n");
        } while (in == 'Y' || in == 'y');

        System.out.print("\n");

        bangunRuang();
    }
    private static void balok(){
        Balok balok;
        double s1, s2, s3;
        char in;

        balok = new Balok();
        do {
            System.out.println("================================================");
            System.out.println("| Balok                                        |");
            System.out.println("================================================");
            System.out.print("Masukan panjang balok: ");
            s1 = scanner.nextDouble();
            balok.setX(s1);
            System.out.print("Masukan lebar balok: ");
            s2 = scanner.nextDouble();
            balok.setY(s2);
            System.out.print("Masukan tinggi balok: ");
            s3 = scanner.nextDouble();
            balok.setZ(s3);
            System.out.println("Volume balok: " + balok.volume());
            System.out.println("Luas Permukaan balok: " + balok.luasPermukaan());

            System.out.print("\nIngin mencoba lagi? (y/t) : ");
            in = scanner.next().charAt(0);
            System.out.print("\n");
        } while (in == 'Y' || in == 'y');

        System.out.print("\n");

        bangunRuang();
    }
    private static void tabung(){
        Tabung tabung;
        double r, t;
        char in;

        tabung = new Tabung();
        do {
            System.out.println("================================================");
            System.out.println("| Tabungan                                     |");
            System.out.println("================================================");
            System.out.print("Masukan jari jari tabung: ");
            r = scanner.nextDouble();
            tabung.setR(r);
            System.out.print("Masukan tinggi tabung: ");
            t = scanner.nextDouble();
            tabung.setT(t);
            System.out.println("Volume tabung: " + tabung.volume());
            System.out.println("Luas Permukaan tabung: " + tabung.luasPermukaan());

            System.out.print("\nIngin mencoba lagi? (y/t) : ");
            in = scanner.next().charAt(0);
            System.out.print("\n");
        } while (in == 'Y' || in == 'y');

        System.out.print("\n");

        bangunRuang();
    }
    private static void kerucut(){
        Kerucut kerucut;
        double r, t;
        char in;

        kerucut = new Kerucut();
        do {
            System.out.println("================================================");
            System.out.println("| Kerucut                                      |");
            System.out.println("================================================");
            System.out.print("Masukan jari jari kerucut: ");
            r = scanner.nextDouble();
            kerucut.setR(r);
            System.out.print("Masukan tinggi kerucut: ");
            t = scanner.nextDouble();
            kerucut.setT(t);
            System.out.println("Volume kerucut: " + kerucut.volume());
            System.out.println("Luas Permukaan kerucut: " + kerucut.luasPermukaan());

            System.out.print("\nIngin mencoba lagi? (y/t) : ");
            in = scanner.next().charAt(0);
            System.out.print("\n");
        } while (in == 'Y' || in == 'y');

        System.out.print("\n");

        bangunRuang();
    }
}